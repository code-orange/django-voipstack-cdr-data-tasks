from django.core.management.base import BaseCommand

from django_voipstack_cdr_data_tasks.django_voipstack_cdr_data_tasks.tasks import (
    voipstack_job_cdr_data_retention,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        voipstack_job_cdr_data_retention()
