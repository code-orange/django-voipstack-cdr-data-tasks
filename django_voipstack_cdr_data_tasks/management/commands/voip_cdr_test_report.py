from datetime import datetime, timedelta, timezone

from django.core.management.base import BaseCommand

from django_voipstack_cdr_data.django_voipstack_cdr_data.func import (
    generate_cdr_data_report,
)
from django_voipstack_trunks.django_voipstack_trunks.models import VoipTrunks


class Command(BaseCommand):
    help = "Generate Test CDR"

    def handle(self, *args, **options):
        for trunk in VoipTrunks.objects.all():
            test_report = generate_cdr_data_report(
                trunk,
                datetime.now(timezone.utc) - timedelta(days=31),
                datetime.now(timezone.utc) - timedelta(days=1),
            )

            with open(
                "temp\\voip_cdr_test_report_" + trunk.trunk_name + ".pdf", "wb"
            ) as f:
                f.write(test_report["pdf"])

            with open(
                "temp\\voip_cdr_test_report_" + trunk.trunk_name + ".csv", "w"
            ) as f:
                f.write(test_report["csv"])

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
