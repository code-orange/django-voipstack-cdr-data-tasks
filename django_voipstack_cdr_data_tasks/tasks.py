from base64 import b64encode
from datetime import timedelta, timezone
from math import ceil

from celery import shared_task
from dateutil.relativedelta import relativedelta
from django.forms import model_to_dict
from django.template import engines

from django_mdat_phone_billing.django_mdat_phone_billing.models import (
    MdatPhoneBillingRouteItems,
)
from django_simple_notifier.django_simple_notifier.models import *
from django_simple_notifier.django_simple_notifier.plugin_zammad import *
from django_voipstack_cdr_data.django_voipstack_cdr_data.func import (
    generate_cdr_data_report,
    get_zone,
)
from django_voipstack_cdr_data.django_voipstack_cdr_data.models import *
from django_voipstack_numfilter.django_voipstack_numfilter.models import *


@shared_task(name="voipstack_job_cdr_data_reports")
def voipstack_job_cdr_data_reports():
    django_engine = engines["django"]

    template_subject_cdr_letter = (
        SnotifierTemplates.objects.get(
            name="subject_cdr_letter"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_ticket_note = (
        SnotifierTemplates.objects.get(
            name="ticket_note"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_general_header = (
        SnotifierTemplates.objects.get(
            name="general_header"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_general_footer = (
        SnotifierTemplates.objects.get(
            name="general_footer"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text
    template_cdr_letter = (
        SnotifierTemplates.objects.get(
            name="cdr_letter"
        ).snotifiertemplatetranslations_set.get(lang_code="de")
    ).template_text

    all_report_trunks = VoipTrunks.objects.exclude(cdr_report=0).all()

    last_full_sync = (
        VoipCdrSyncState.objects.all()
        .values("sync_state_date")
        .order_by("sync_state_date")
        .first()["sync_state_date"]
    )

    for trunk in all_report_trunks:
        date_ranges = list()

        next_cut = date(trunk.cdr_report_date.year, trunk.cdr_report_date.month, 27)

        if trunk.cdr_report_date < next_cut:
            date_ranges.append((trunk.cdr_report_date, next_cut))

        while True:
            start_data = next_cut
            next_cut = next_cut + relativedelta(months=1)

            if next_cut > date.today():
                break

            date_ranges.append((start_data, next_cut))

        for date_range in date_ranges:
            start_date = date_range[0]
            end_date = date_range[1]

            if end_date > last_full_sync.date():
                continue

            cdr_report = generate_cdr_data_report(
                trunk,
                datetime(
                    start_date.year,
                    start_date.month,
                    start_date.day,
                    tzinfo=timezone.utc,
                ),
                datetime(
                    end_date.year, end_date.month, end_date.day, tzinfo=timezone.utc
                ),
            )

            end_date_filename = end_date - timedelta(days=1)

            filename_base = (
                "CDR_Report_"
                + start_date.strftime("%Y%m%d")
                + "_"
                + end_date_filename.strftime("%Y%m%d")
            )

            attachments = list()

            attachments.append(
                {
                    "filename": filename_base + ".pdf",
                    "data": b64encode(cdr_report["pdf"]).decode("utf-8"),
                    "mime-type": "application/pdf",
                }
            )

            attachments.append(
                {
                    "filename": filename_base + ".csv",
                    "data": b64encode(cdr_report["csv"].encode("utf-8")).decode(
                        "utf-8"
                    ),
                    "mime-type": "text/csv",
                }
            )

            notifier_list = list()
            cdr_recipient = SnotifierEmailContactZammad()
            cdr_recipient.email = trunk.cdr_report_email
            notifier_list.append(cdr_recipient)

            template_opts = dict()
            template_opts["customer"] = model_to_dict(trunk.customer)

            engine_subject = django_engine.from_string(template_subject_cdr_letter)

            engine_note = django_engine.from_string(
                "\r\n".join([template_general_header, template_ticket_note])
            )

            engine_text = django_engine.from_string(
                "\r\n".join(
                    [
                        template_general_header,
                        template_cdr_letter,
                        template_general_footer,
                    ]
                )
            )

            ticket_id = send(
                notifier_list=notifier_list,
                subject=engine_subject.render(template_opts),
                text=engine_text.render(template_opts),
                note=engine_subject.render(template_opts),
                main_address=trunk.cdr_report_email,
                attachments=attachments,
            )

            trunk.cdr_report_date = end_date
            trunk.save()

    return


@shared_task(name="voipstack_job_cdr_data_retention")
def voipstack_job_cdr_data_retention():
    removable_cdrs = VoipCdr.objects.filter(
        call_end__lte=datetime.now() - relativedelta(months=6)
    ).all()
    removable_cdrs.delete()

    return


@shared_task(name="voipstack_job_cdr_data_assign_zone")
def voipstack_job_cdr_data_assign_zone():
    for cdr in VoipCdr.objects.filter(zone_id__isnull=True).order_by("id"):
        cdr.zone = get_zone(cdr.num_dst_obj)
        cdr.save()

    return


@shared_task(name="voipstack_job_cdr_data_assign_billing_units")
def voipstack_job_cdr_data_assign_billing_units():
    for cdr in (
        VoipCdr.objects.filter(billing_units__isnull=True)
        .exclude(zone_id__isnull=True)
        .order_by("id")
    ):
        call_billsec = cdr.call_billsec

        # check if special route
        special_routes = list(MdatPhoneBillingRouteItems.objects.filter(item=cdr.zone))

        if len(special_routes) > 0:
            special_route = special_routes[0]

            if special_route.billing_type == 200:
                cdr.billing_units = 1
                cdr.save()
                continue

            if special_route.free_time > 0:
                call_billsec = cdr.call_billsec - special_route.free_time

                if call_billsec < 0:
                    call_billsec = 0

        # round to full seconds
        call_billsec = ceil(call_billsec)

        # round to full minutes, save billing units
        cdr.billing_units = ceil(call_billsec / 60)

        cdr.save()

    return
